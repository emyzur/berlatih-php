<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Tukar Besar Kecil</h1>
    <?php
    function tukar_besar_kecil($string){
        //kode di sini
        $tampung ="";
        for ($i=0; $i <strlen($string) ; $i++) { 
            if (ctype_upper($string[$i])) {
                $tampung .= strtolower($string[$i]);
            }else{
                $tampung .= strtoupper($string[$i]);
            }
        }
        return $tampung. "<br>";
        }
        
        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>
</body>
</html>